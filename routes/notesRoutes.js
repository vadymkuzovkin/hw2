const express = require('express');

const notesController = require('../controllers/notesController');

const router = express.Router();


router.get('/:id', notesController.getNoteByIdForUser);
router.put('/:id', notesController.updateNoteByIdForUser);
router.patch('/:id', notesController.checkUnchekNoteByIdForUser);
router.delete('/:id', notesController.deleteNoteByIdForUser);
router.post('/', notesController.postNoteForUser);
router.get('/', notesController.getNotesForUser);


module.exports = router;
