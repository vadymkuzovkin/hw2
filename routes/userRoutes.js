const express = require('express');
const userController = require('../controllers/userController');

const router = express.Router();

router.get('/api/users/me', userController.getProfile);
router.delete('/api/users/me', userController.deleteProfile);
router.patch('/api/users/me', userController.patchProfile);

module.exports = router;
