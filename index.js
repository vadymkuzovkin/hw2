const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const authMiddleware = require('./middlewares/authMiddleware');

const authRoutes = require('./routes/authRoutes');
const userRoutes = require('./routes/userRoutes');
const notesRoutes = require('./routes/notesRoutes');

const app = express();

app.use(morgan('tiny'));

app.use(express.json({ extended: true }));

app.use('/api/auth', authRoutes);
app.use(authMiddleware);
app.use(userRoutes);
app.use('/api/notes', notesRoutes);

app.use((req, res) => {
	res.status(404).send({ message: 'Page not found!' });
});

app.use((err, req, res, next) => {
	res.status(500).send({ message: 'Server error' });
});

const start = async () => {
	try {
		await mongoose.connect('mongodb+srv://Vadym:5355493@firstcluster.risy7.mongodb.net/node_hw2?retryWrites=true&w=majority',
			{ useNewUrlParser: true, useUnifiedTopology: true });
		console.log('Connected')
		app.listen(process.env.PORT || 8080);
	} catch (error) {
		res.status(500).send({ message: error.message });
	}
};

start();

