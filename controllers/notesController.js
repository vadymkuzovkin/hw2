const Note = require('../models/noteModel');

exports.getNotesForUser = async (req, res) => {
	try {
		const { userId } = req.user;

		const queryOffset = +req.query.offset || 0;
		const queryLimit = +req.query.limit || 0;
		const count = await Note.count();

		const notesList = await Note.find({ userId }).select('-__v')
			.skip(queryOffset).limit(queryLimit);

		return res.status(200).send({
			offset: queryOffset,
			limit: queryLimit,
			count,
			notes: notesList
		});
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.postNoteForUser = async (req, res) => {
	try {
		const { userId } = req.user;
		const noteText = req.body.text;
		const note = new Note({
			userId,
			completed: false,
			text: noteText
		});

		await note.save();
		return res.status(200).send({ message: 'Success' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.getNoteByIdForUser = async (req, res) => {
	try {
		const noteId = req.params.id;
		const { userId } = req.user;

		const note = await Note.findOne({ _id: noteId, userId }).select('-__v');

		res.status(200).send({ note });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.updateNoteByIdForUser = async (req, res) => {
	try {
		const noteId = req.params.id;
		const { userId } = req.user;
		const newNoteText = req.body.text;

		await Note.findOneAndUpdate({ _id: noteId, userId }, { text: newNoteText });

		res.status(200).send({ message: 'Success' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.checkUnchekNoteByIdForUser = async (req, res) => {
	try {
		const noteId = req.params.id;
		const { userId } = req.user;

		const note = await Note.findOne({ _id: noteId, userId });
		note.completed = !note.completed;

		note.save();

		res.status(200).send({ message: 'Success' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.deleteNoteByIdForUser = async (req, res) => {
	try {
		const noteId = req.params.id;
		// const { userId } = req.user;

		await Note.findByIdAndRemove(noteId);

		res.status(200).send({ message: 'Success' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

