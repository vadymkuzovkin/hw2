const User = require('../models/userModel');
const bcrypt = require('bcrypt');

exports.getProfile = async (req, res) => {
	try {
		const userId = req.user.userId;
		const userInfo = await User.findById(userId);
		res.status(200).send({
			user: {
				_id: userInfo._id,
				username: userInfo.username,
				createdDate: userInfo.createdDate
			}
		});
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.deleteProfile = async (req, res) => {
	try {
		const userId = req.user.userId;
		await User.findByIdAndRemove(userId);
		res.status(200).send({ message: 'User deleted successfully' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};

exports.patchProfile = async (req, res) => {
	try {
		const userId = req.user.userId;
		const user = await User.findById(userId);
		const { oldPassword, newPassword } = req.body;

		if (!(await bcrypt.compare(oldPassword, user.password))) {
			return res.status(200)
				.send({ message: 'Please enter correct password!' });
		}

		user.password = await bcrypt.hash(newPassword, 10);
		user.save();
		res.status(200).send({ message: 'Password was successfully changed' });
	} catch (error) {
		res.status(400).send({ message: error.message });
	}
};
